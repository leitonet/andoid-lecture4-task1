package com.example.l4task1;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class MainActivity extends AppCompatActivity {

    Button timer;
    Button stopwatch;
    TextView textView;
    TimerFragment timerFragment;
    StopwatchFragment stopwatchFragment;
    FragmentTransaction fTrans;

    Unbinder unbinder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        timerFragment = new TimerFragment();
        stopwatchFragment = new StopwatchFragment();
        textView = (TextView) findViewById(R.id.text_view);
        unbinder = ButterKnife.bind(this);

        timer = (Button) findViewById(R.id.timer);
        timer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                textView.setText("Timer");
                fTrans = getFragmentManager().beginTransaction();
                fTrans.replace(R.id.fragment, timerFragment);
                fTrans.commit();
            }
        });


        stopwatch = (Button) findViewById(R.id.stopwatch);
        stopwatch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                textView.setText("Stopwatch");
                fTrans = getFragmentManager().beginTransaction();
                fTrans.replace(R.id.fragment, stopwatchFragment);
                fTrans.commit();
            }
        });
    }


}
