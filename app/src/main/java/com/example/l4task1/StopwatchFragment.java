package com.example.l4task1;

import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Fragment;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnLongClick;


public class StopwatchFragment extends Fragment {

    public static final String TIME = "time";
    String value;
    public static final int SECONDS = 0;
    int numberOfClick = 0;

    @BindView(R.id.stopwatch_text_view)
    TextView textView;

    MyStopwatch myStopwatch;

    public static StopwatchFragment newInstance(String timer) {

        Bundle args = new Bundle();
        args.putString(TIME, timer);
        StopwatchFragment stopwatchFragment = new StopwatchFragment();
        stopwatchFragment.setArguments(args);
        return stopwatchFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null && getArguments().containsKey(TIME)) {
            value = getArguments().getString(TIME);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.stopwatch_fragment, container, false);
        ButterKnife.bind(this, view);
        textView.setText(SECONDS + "");
        return view;
    }


    @OnClick(R.id.stopwatch_text_view)
    void onClick(){
        numberOfClick ++;

        if(numberOfClick == 1) {
            myStopwatch = (MyStopwatch) new MyStopwatch(Integer.parseInt(textView.getText().toString())).execute();
        }else if(numberOfClick == 2){
            myStopwatch.cancel(true);
            numberOfClick = 0;
        }
    }

    @OnLongClick(R.id.stopwatch_text_view)
    boolean OnLongClick(){
        numberOfClick = 0;
        myStopwatch.cancel(true);
        textView.setText(SECONDS + "");
        return true;
    }

    class MyStopwatch extends AsyncTask<Integer, Integer, Void> {

        public int seconds;

        MyStopwatch(int seconds){
            this.seconds = seconds;
        }

        @Override
        protected Void doInBackground(Integer... ints) {
            while(true) {
                if (seconds >= 0 && isCancelled()) {
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    seconds++;
                    publishProgress(seconds);
                }else {
                    break;
                }
            }
            return null;
        }

        @Override
        protected void onPreExecute() {
            textView.setText(seconds + "");
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            textView.setText(seconds + "");
        }

        @Override
        protected void onProgressUpdate(Integer... ints) {
            textView.setText(ints[0] + "");
        }
    }


}
