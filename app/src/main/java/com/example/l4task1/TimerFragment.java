package com.example.l4task1;

import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnLongClick;


public class TimerFragment extends Fragment {

    public static final String TIMER = "timer";
    public static final String TIME = "time";
    public static int startSeconds = 0;

    int numberOfClick = 0;
    String value;

    @BindView(R.id.timer_text_view)
    TextView textView;


    MyTimer myTimer;

    public static TimerFragment newInstance(String timer) {

        Bundle args = new Bundle();
        args.putString(TIMER, timer);
        TimerFragment timerFragment = new TimerFragment();
        timerFragment.setArguments(args);
        return timerFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null && getArguments().containsKey(TIMER)) {
            value = getArguments().getString(TIMER);
        }

    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.timer_fragment, container, false);
        ButterKnife.bind(this, view);

        if (savedInstanceState != null) {
            textView.setText(savedInstanceState.getString("time"));
        }
        startSeconds = Integer.parseInt(textView.getText().toString());
        if (startSeconds == 0) {
            final EditText setTime = new EditText(getActivity());
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setTitle("Timer");
            builder.setMessage("Please set the time");
            builder.setView(setTime);
            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    startSeconds = Integer.parseInt(setTime.getText().toString());
                    textView.setText(startSeconds + "");
                }
            });
            builder.create();
            builder.show();
        }

        return view;
    }

    @OnClick(R.id.timer_text_view)
    void onClick() {
        numberOfClick++;

        if (numberOfClick == 1) {
            myTimer = (MyTimer) new MyTimer(Integer.parseInt(textView.getText().toString())).execute();
        } else if (numberOfClick == 2) {
            myTimer.cancel(true);
            numberOfClick = 0;
        }
    }

    @OnLongClick(R.id.timer_text_view)
    boolean OnLongClick() {
        numberOfClick = 0;
        myTimer.cancel(true);
        textView.setText(startSeconds + "");
        return false;
    }

    class MyTimer extends AsyncTask<Integer, Integer, Void> {

        public int seconds = 0;

        public MyTimer(int seconds) {
            this.seconds = seconds;
        }


        @Override
        protected Void doInBackground(Integer... ints) {
            while (true) {
                if (!(seconds <= 0) && !isCancelled()) {
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    seconds--;
                    publishProgress(seconds);
                } else {
                    break;
                }
            }
            return null;
        }

        @Override
        protected void onPreExecute() {
            textView.setText(seconds + "");
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            textView.setText(seconds + "");
        }

        @Override
        protected void onProgressUpdate(Integer... ints) {
            textView.setText(ints[0] + "");
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("time", textView.getText().toString());
    }
}
